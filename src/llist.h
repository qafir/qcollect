/*
 *    Qcollect: C collections library
 *    Copyright (C) 2015  Alfredo Mungo <alfredo.mungo@openmailbox.org>
 *
 *    This program is free software; you shall redistribute it and/or modify
 *    it under the terms of the GNU General Public License version 2 as
 *    published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License along
 *    with this program; if not, write to the Free Software Foundation, Inc.,
 *    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
#ifndef _QCOLLECT_LLIST_H
#define _QCOLLECT_LLIST_H

#include "common.h"

struct QCOLLECT_LLIST_NODE {
  union QCOLLECT_VALUE val;
  struct QCOLLECT_LLIST_NODE *prev, *next;
};

struct QCOLLECT_LLIST {
  struct QCOLLECT_LLIST_NODE *first, *last;
};

typedef struct QCOLLECT_LLIST_NODE QC_LL_NODE;
typedef struct QCOLLECT_LLIST QC_LL;

/*
 * Initializes a new linked list instance.
 * The returned value should be freed with qc_ll_free().
 *
 * Return value:
 *  A new linked list object pointer
 */
QC_LL *qc_ll_init();

/*
 * Frees any memory associated with a linked list instance.
 *
 * Arguments:
 *  ll: A pointer to the linked list object to free
 */
void qc_ll_free(QC_LL *ll);

/*
 * Inserts a node after another.
 *
 * Arguments:
 *  ll: The linked list object pointer
 *  value: The value to assign to the new item
 *  node: The node after which to perform the insertion
 */
QC_LL_NODE *qc_ll_insert_after(QC_LL *restrict ll, QC_VALUE value, QC_LL_NODE *restrict node);

/*
 * Inserts a node before another.
 *
 * Arguments:
 *  ll: The linked list object pointer
 *  value: The value to assign to the new item
 *  node: The node before which to perform the insertion
 */
QC_LL_NODE *qc_ll_insert_before(QC_LL *ll, QC_VALUE value, QC_LL_NODE *node);

/*
 * Appends an item at the end of the list.
 *
 *  ll: The linked list object pointer
 *  value: The value to assign to the new item
 */
#define qc_ll_append(ll, value) qc_ll_insert_after((ll), (value), (ll)->last)

/*
 * Removes an item from the list.
 *
 * Arguments:
 *  ll: The linked list object pointer
 *  node: A pointer to the node to remove (after this function returns, node is freed and shell not be accessed again)
 */
void qc_ll_remove(QC_LL *restrict ll, QC_LL_NODE *restrict node);

#endif /* _QCOLLECT_LLIST_H */
