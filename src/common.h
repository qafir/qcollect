/*
 *    Qcollect: C collections library
 *    Copyright (C) 2015  Alfredo Mungo <alfredo.mungo@openmailbox.org>
 *
 *    This program is free software; you shall redistribute it and/or modify
 *    it under the terms of the GNU General Public License version 2 as
 *    published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License along
 *    with this program; if not, write to the Free Software Foundation, Inc.,
 *    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
#ifndef _QCOLLECT_COMMON_H
#define _QCOLLECT_COMMON_H

#define QCOLLECT_VERSION_MAJOR 0
#define QCOLLECT_VERSION_MINOR 2

#include <stdbool.h>
#include <complex.h>

union QCOLLECT_VALUE {
  char c;

  /* Signed integer types */
  signed char sc;
  short int s;
  int i;
  long int l;
  long long int ll;

  /* Unsigned integer types */
  _Bool b;
  unsigned char uc;
  unsigned short int us;
  unsigned int ui;
  unsigned long int ul;
  unsigned long long ull;

  /* Floating point types */
  float f;
  double d;
  long double ld;

  /* Complex types */
  float _Complex fc;
  double _Complex dc;
  long double _Complex ldc;

  /* Pointer types */
  void *ptr;
};

typedef union QCOLLECT_VALUE QC_VALUE;

#endif /* _QCOLLECT_COMMON_H */
