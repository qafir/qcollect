#DEBUG=1
CC=gcc
CFLAGS=-std=c11
AR=ar # ?
ARFLAGS=

DESTDIR=/usr/local
TESTDIR=test
SRCDIR=src
export SRCPATH=$(abspath $(SRCDIR))

ifdef windir
TMPDIR=.
else
TMPDIR=/tmp
endif

LIBNAME=qcollect
LIBFILE=lib$(LIBNAME).a
export LIBPATH=$(abspath $(LIBFILE))

MODULES=llist alist
OBJECTS=$(foreach m,$(MODULES),$(SRCDIR)/$(m).o)
HEADERS=$(foreach m,$(MODULES),$(SRCDIR)/$(m).h)

INSTALL_LIBDIR=$(DESTDIR)/lib
INSTALL_INCDIR=$(DESTDIR)/include/$(LIBNAME)

ifeq ($(DEBUG),1)
CFLAGS+=-ggdb -Werror
else
CFLAGS+=-g0 -Wall -DNDEBUG -O3
endif

.PHONY: clean test install dist

all: $(LIBFILE)

$(LIBFILE): $(OBJECTS)
	$(AR) rcs $@ $^

$(OBJECTS): %.o: %.c
	$(CC) -c $(CFLAGS) -o $@ $<

clean:
	$(MAKE) -C $(TESTDIR) clean
	$(RM) $(LIBFILE) $(OBJECTS)

distclean: clean
	$(RM) VERSION $(LIBNAME)-*.tar.gz

test: $(LIBFILE)
	$(MAKE) -C $(TESTDIR) run

install: $(LIBFILE) $(HEADERS)
	install -m 755 -d $(INSTALL_LIBDIR) $(INSTALL_INCDIR)
	install -m 644 $(LIBFILE) $(INSTALL_LIBDIR)/$(LIBFILE)
	install -m 644 $(HEADERS) $(INSTALL_INCDIR)/
	install -m 644 $(SRCDIR)/common.h $(INSTALL_INCDIR)/

dist: VERSION
	OUTDIR="$(LIBNAME)-$$(<VERSION)" && \
	install -m 755 -d $$OUTDIR/$(SRCDIR) $$OUTDIR/$(TESTDIR) && \
	install -m 644 $(SRCDIR)/*.h $(SRCDIR)/*.c $$OUTDIR/src && \
	install -m 644 $(TESTDIR)/Makefile $(TESTDIR)/*.c $$OUTDIR/test && \
	install -m 644 Makefile LICENSE $$OUTDIR && \
	tar cf - $$OUTDIR | gzip -c > $$OUTDIR.tar.gz && \
	$(RM) -r $$OUTDIR

VERSION: $(SRCDIR)/common.h
	awk '/#define QCOLLECT_VERSION_MAJOR/ { major=$$3 } /#define QCOLLECT_VERSION_MINOR/ { minor=$$3 } END { print(major "." minor) }' $< >$@
